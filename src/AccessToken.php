<?php

namespace JdVopClient;

use Exception;
use JdVopClient\Utils\Curl;
use JdVopClient\Utils\Helper;

class AccessToken
{
    public  $log_text = [];
    private $private_key;
    private $username;
    private $password;
    private $appkey;
    private $appsecret;
    private $redirect_uri;
    private $refresh_token;

    public function SetUserName($username)
    {
        $this->username = $username;
    }

    public function SetPassword(string $password)
    {
        $this->password = $password;
    }

    public function SetAppKey(string $appkey)
    {
        $this->appkey = $appkey;
    }

    public function SetAppSecret($appsecret)
    {
        $this->appsecret = $appsecret;
    }

    public function SetRedirectUri($redirect_uri)
    {
        $this->redirect_uri = $redirect_uri;
    }

    public function SetPrivateKey(string $private_key)
    {
        $this->private_key = $private_key;
    }

    public function SetRefreshToken(string $refresh_token)
    {
        $this->refresh_token = $refresh_token;
    }


    /**
     * 构建授权Url
     * @return string
     * @throws Exception
     */
    public function buildOauthUrl(): string
    {
        // 密码进行加密
        $urlPassword = Helper::encodeBase64URLSafe(
            Helper::rsaEncryptWithOpenSSL($this->private_key, md5($this->password))
        );

        // 开放授权参数
        $oauthParams = [
            'app_key'       => $this->appkey,
            'redirect_uri'  => $this->redirect_uri,
            'username'      => $this->username,
            'password'      => $urlPassword,
            'response_type' => 'code',
            'scope'         => 'snsapi_base'
        ];

        // 构建授权链接并返回
        $baseUrl = 'https://open-oauth.jd.com/oauth2/authorizeForVOP';
        return $baseUrl . '?' . http_build_query($oauthParams);
    }

    /**
     * 使用Code换取AccessToken
     * @param $code
     * @return bool|string
     * @throws Exception
     */
    public function code2AccessToken($code)
    {
        // 换取Token参数
        $oauthParams = [
            'app_key'    => $this->appkey,
            'app_secret' => $this->appsecret,
            'grant_type' => 'authorization_code',
            'code'       => $code
        ];

        // 发起一次Get请求来获取信息
        $responseData = $this->httpGet('https://open-oauth.jd.com/oauth2/access_token', $oauthParams);
        if ($responseData['code'] != 0) {
            throw new Exception('Code to session error[' . $responseData['code'] . ']: ' . $responseData['msg']);
        }

        return $responseData;
    }

    /**
     * 获取访问授权令牌
     * @return bool|string
     * @throws Exception
     */
    public function GetAccessToken()
    {
        $this->log_text[] = 'GetAccessToken';
        try {
            // 在后端发起一次授权链接访问
            $authUrl        = $this->buildOauthUrl();
            $this->log_text[] = $authUrl;
            $response       = $this->httpGet($authUrl, [], true);
            $this->log_text[] = json_encode($response);
            // 页面访问后发起一次302则进入跳转链接 获取Code
            $location       = $response->getResponseHeaders('location');
            $this->log_text[] = json_encode($location);
            parse_str(parse_url($location)['query'], $locationQuery);
            if (!array_key_exists('code', $locationQuery)) {
                throw new Exception('Business error: Authorization link does not carry code.');
            }
            // 使用授权Code换取授权令牌并发起缓存 注意: 令牌需要提前一点儿刷新
            $accessToken    = $this->code2AccessToken($locationQuery['code']);
            $this->log_text[] = $response;
            return $accessToken;
        } catch (\Throwable $th) {
            $this->log_text[] = 'ERROR:' . $th->getMessage();
            throw new Exception($th->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function RefreshToken()
    {
        $this->log_text[] = 'RefreshToken';
        try {
            // 换取Token参数
            $refreshParams  = [
                'app_key'       => $this->appkey,
                'app_secret'    => $this->appsecret,
                'grant_type'    => 'refresh_token',
                'refresh_token' => $this->refresh_token
            ];
            $baseUrl        = 'https://open-oauth.jd.com/oauth2/refresh_token';
            $url            = $baseUrl . '?' . http_build_query($refreshParams);
            $this->log_text[] = $url;
            // 发起一次Get请求来获取信息
            $responseData   = $this->httpGet($url);
            $this->log_text[] = json_encode($responseData);
            if ($responseData['code'] != 0) {
                throw new Exception('RefreshToken Code to session error[' . $responseData['code'] . ']: ' . $responseData['msg']);
            }

            return $responseData;
        } catch (\Throwable $th) {
            $this->log_text[] = $th->getMessage();
            throw new Exception($th->getMessage());
        }
    }

    /**
     * 发起一次Get请求
     * @param string $url 请求地址
     * @param array $queryParams 查询参数
     * @param bool $noParse 无需解析直接返回
     * @throws Exception
     */
    private function httpGet(string $url, array $queryParams = [], bool $noParse = false)
    {
        // 发起一次Curl请求
        $curl = new Curl;
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
        $curl->setOpt(CURLOPT_TIMEOUT, 10);
        $curl->setUserAgent('PHP Curl/2.3 Via JdVopSdk');
        $curlResponse = $curl->get($url, $queryParams);

        // 返回已解码的信息
        return $noParse ? $curlResponse : $this->parseResponse($curlResponse);
    }

    /**
     * 解析响应内容
     * @param Curl $curlResponse
     * @return mixed
     * @throws Exception
     */
    private function parseResponse(Curl $curlResponse)
    {
        // 请求不成功
        if (!$curlResponse->isSuccess()) {
            $httpStatus = $curlResponse->getHttpStatus();
            throw new Exception('Abnormal response status: ' . $httpStatus);
        }

        // 返回的内容为空
        if (empty($curlResponse->getResponse())) {
            throw new Exception('Abnormal response: response body is empty.');
        }

        // 返回数据不合法
        return Helper::jsonDecode($curlResponse->getResponse());
    }
}