<?php

namespace JdVopClient\Utils;

use Exception;

/**
 * 助手函数
 * @class Helper
 * @package Jsrx\JdVopSdk\Utils
 */
class Helper
{
    /**
     * 生成随机字母数字字符串
     * @param int $length
     * @param string $alphabet
     * @return false|string
     */
    public static function randStr(int $length = 6, string $alphabet = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789')
    {
        // 随机之前先进行播种
        mt_srand();
        // 重复字母表以防止生成长度溢出字母表长度
        if ($length >= strlen($alphabet)) {
            $rate = intval($length / strlen($alphabet)) + 1;
            $alphabet = str_repeat($alphabet, $rate);
        }
        // 打乱顺序返回
        return substr(str_shuffle($alphabet), 0, $length);
    }

    /**
     * URL安全的Base64编码
     * @param $content
     * @return string|string[]
     */
    public static function encodeBase64URLSafe($content)
    {
        $data = base64_encode($content);
        return str_replace(array( '+', '/', '=' ), array( '-', '_', '' ), $data);
    }

    /**
     * URL安全的Base64解码
     * @param $base64
     * @return false|string
     */
    public static function decodeBase64URLSafe($base64)
    {
        $data = str_replace(array( '-', '_' ), array( '+', '/' ), $base64);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    /**
     * 进行Json编码
     * @param mixed $value 需要编码的内容
     * @param int $options 编码选项
     * @param int $depth 最大编码深度
     * @return false|string
     * @throws Exception
     */
    public static function jsonEncode($value, int $options = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT, int $depth = 512)
    {
        $json = json_encode($value, $options, $depth);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new Exception(
                'json_encode error: ' . json_last_error_msg()
            );
        }

        return $json;
    }

    /**
     * 进行Json解码
     * @param string $json 需要解码的内容
     * @param bool $assoc 是否解码为关联数组
     * @param int $depth 解码最大深度
     * @param int $options 解码选项
     
     * @throws Exception
     */
    public static function jsonDecode(string $json, bool $assoc = true, int $depth = 512, int $options = 0)
    {
        $data = json_decode($json, $assoc, $depth, $options);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new Exception(
                'json_decode error: ' . json_last_error_msg()
            );
        }

        return $data;
    }

    /**
     * 使用OpenSSL库进行Rsa加密
     * @param string $rsaPrivateKey
     * @param string $plaintext
     * @return string
     * @throws Exception
     */
    public static function rsaEncryptWithOpenSSL(string $rsaPrivateKey, string $plaintext): string
    {
        // 如果这是一个路径，读入内容
        if (is_file($rsaPrivateKey)) {
            $rsaPrivateKey = file_get_contents($rsaPrivateKey);
        }

        // 私钥格式化
        $rsaPemFormat = "-----BEGIN PRIVATE KEY-----\n%s-----END PRIVATE KEY-----\n";
        $rsaPrivatePem = sprintf($rsaPemFormat, chunk_split($rsaPrivateKey, 64, "\n"));
        $sslPrivate = openssl_pkey_get_private($rsaPrivatePem);

        // 校验私钥是否合法
        if (!$sslPrivate) {
            throw new Exception(
                'Rsa key invalid: ' . openssl_error_string()
            );
        }

        // 取得合法的私钥后进行加密
        if (!openssl_private_encrypt($plaintext, $encData, $sslPrivate)) {
            throw new Exception(
                'Rsa encrypt error: ' . openssl_error_string()
            );
        }

        return $encData;
    }
}