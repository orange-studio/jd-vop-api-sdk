<?php

namespace JdVopClient\request;

class VopGoodsApplyReserveRequest
{
    private $apiParas = array();
    private $version;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.applyReserve";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function setReserveInfo($reserveInfo)
    {
        $this->apiParas['reserveInfo'] = $reserveInfo;
    }

    public function getReserveInfo()
    {
        return $this->apiParas['reserveInfo'];
    }
}
