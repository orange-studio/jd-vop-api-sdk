<?php

namespace JdVopClient\request\domain\VopAfsUpdateSendInfo;

class WaybillInfoVoOpenReq
{
    private $params = array();
    private $deliverDate;
    private $wareNum;
    private $expressCode;
    private $wareId;
    private $wareType;
    private $expressCompany;
    private $freightMoney;

    function __construct()
    {}

    public function getDeliverDate()
    {
        return $this->deliverDate;
    }

    public function setDeliverDate($deliverDate)
    {
        $this->params['deliverDate'] = $deliverDate;
    }

    public function getWareNum()
    {
        return $this->wareNum;
    }

    public function setWareNum($wareNum)
    {
        $this->params['wareNum'] = $wareNum;
    }

    public function getExpressCode()
    {
        return $this->expressCode;
    }

    public function setExpressCode($expressCode)
    {
        $this->params['expressCode'] = $expressCode;
    }

    public function getWareId()
    {
        return $this->wareId;
    }

    public function setWareId($wareId)
    {
        $this->params['wareId'] = $wareId;
    }

    public function getWareType()
    {
        return $this->wareType;
    }

    public function setWareType($wareType)
    {
        $this->params['wareType'] = $wareType;
    }

    public function getExpressCompany()
    {
        return $this->expressCompany;
    }

    public function setExpressCompany($expressCompany)
    {
        $this->params['expressCompany'] = $expressCompany;
    }

    public function getFreightMoney()
    {
        return $this->freightMoney;
    }

    public function setFreightMoney($freightMoney)
    {
        $this->params['freightMoney'] = $freightMoney;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
