<?php

namespace JdVopClient\request\domain\VopAfsUpdateSendInfo;

class UpdateAfterSaleWayBillOpenReq
{
    private $params = array();
    private $thirdApplyId;
    private $orderId;
    private $customerPin;

    function __construct()
    {}

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->params['thirdApplyId'] = $thirdApplyId;
    }

    public function setWaybillInfoVoOpenReqList($waybillInfoVoOpenReqList)
    {
        $size = count($waybillInfoVoOpenReqList);
        for ($i = 0; $i < $size; $i++) {
            $waybillInfoVoOpenReqList [$i] = $waybillInfoVoOpenReqList [$i]->getInstance();
        }
        $this->params['waybillInfoVoOpenReqList'] = $waybillInfoVoOpenReqList;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->params['orderId'] = $orderId;
    }

    public function getCustomerPin()
    {
        return $this->customerPin;
    }

    public function setCustomerPin($customerPin)
    {
        $this->params['customerPin'] = $customerPin;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
