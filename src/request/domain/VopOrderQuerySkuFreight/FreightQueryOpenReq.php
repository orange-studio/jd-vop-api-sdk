<?php

namespace JdVopClient\request\domain\VopOrderQuerySkuFreight;

class FreightQueryOpenReq
{
    private $params = array();
    private $paymentType;

    function __construct()
    {}

    public function getPaymentType()
    {
        return $this->paymentType;
    }

    public function setPaymentType($paymentType)
    {
        $this->params['paymentType'] = $paymentType;
    }

    public function setSkuInfoList($skuInfoList)
    {
        $size = count($skuInfoList);
        for ($i = 0; $i < $size; $i++) {
            $skuInfoList [$i] = $skuInfoList [$i]->getInstance();
        }
        $this->params['skuInfoList'] = $skuInfoList;
    }

    public function setAreaInfo($areaInfo)
    {
        $this->params['areaInfo'] = $areaInfo->getInstance();
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
