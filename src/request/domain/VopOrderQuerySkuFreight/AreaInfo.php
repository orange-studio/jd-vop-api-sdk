<?php

namespace JdVopClient\request\domain\VopOrderQuerySkuFreight;

class AreaInfo
{
    private $params = array();
    private $provinceId;
    private $cityId;
    private $countyId;
    private $townId;

    function __construct()
    {}

    public function getProvinceId()
    {
        return $this->provinceId;
    }

    public function setProvinceId($provinceId)
    {
        $this->params['provinceId'] = $provinceId;
    }

    public function getCityId()
    {
        return $this->cityId;
    }

    public function setCityId($cityId)
    {
        $this->params['cityId'] = $cityId;
    }

    public function getCountyId()
    {
        return $this->countyId;
    }

    public function setCountyId($countyId)
    {
        $this->params['countyId'] = $countyId;
    }

    public function getTownId()
    {
        return $this->townId;
    }

    public function setTownId($townId)
    {
        $this->params['townId'] = $townId;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
