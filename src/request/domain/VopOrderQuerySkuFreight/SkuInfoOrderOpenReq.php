<?php

namespace JdVopClient\request\domain\VopOrderQuerySkuFreight;

class SkuInfoOrderOpenReq
{
    private $params = array();
    private $skuNum;
    private $skuId;

    function __construct()
    {}

    public function getSkuNum()
    {
        return $this->skuNum;
    }

    public function setSkuNum($skuNum)
    {
        $this->params['skuNum'] = $skuNum;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->params['skuId'] = $skuId;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
