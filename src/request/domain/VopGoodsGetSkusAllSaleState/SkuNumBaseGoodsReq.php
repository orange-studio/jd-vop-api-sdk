<?php

namespace JdVopClient\request\domain\VopGoodsGetSkusAllSaleState;

class SkuNumBaseGoodsReq
{
    private $params = array();
    private $skuNumber;
    private $skuId;

    function __construct()
    {}

    public function getSkuNumber()
    {
        return $this->skuNumber;
    }

    public function setSkuNumber($skuNumber)
    {
        $this->params['skuNumber'] = $skuNumber;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->params['skuId'] = $skuId;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
