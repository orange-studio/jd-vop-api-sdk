<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class SubmitOrderOpenReq
{
    private $params = array();
    private $customerName;
    private $remark;
    private $poNo;
    private $submitStateType;
    private $thirdOrderId;
    private $cpin;

    function __construct()
    {}

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function setCustomerName($customerName)
    {
        $this->params['customerName'] = $customerName;
    }

    public function getRemark()
    {
        return $this->remark;
    }

    public function setRemark($remark)
    {
        $this->params['remark'] = $remark;
    }

    public function getPoNo()
    {
        return $this->poNo;
    }

    public function setPoNo($poNo)
    {
        $this->params['poNo'] = $poNo;
    }

    public function getSubmitStateType()
    {
        return $this->submitStateType;
    }

    public function setSubmitStateType($submitStateType)
    {
        $this->params['submitStateType'] = $submitStateType;
    }

    public function getThirdOrderId()
    {
        return $this->thirdOrderId;
    }

    public function setThirdOrderId($thirdOrderId)
    {
        $this->params['thirdOrderId'] = $thirdOrderId;
    }

    public function setSkuInfoList($skuInfoList)
    {
        $size = count($skuInfoList);
        for ($i = 0; $i < $size; $i++) {
            $skuInfoList [$i] = $skuInfoList [$i]->getInstance();
        }
        $this->params['skuInfoList'] = $skuInfoList;
    }

    public function setPaymentInfo($paymentInfo)
    {
        $this->params['paymentInfo'] = $paymentInfo->getInstance();
    }

    public function setConsigneeInfo($consigneeInfo)
    {
        $this->params['consigneeInfo'] = $consigneeInfo->getInstance();
    }

    public function setInvoiceInfo($invoiceInfo)
    {
        $this->params['invoiceInfo'] = $invoiceInfo->getInstance();
    }

    public function setCustomOrderExt($customOrderExt)
    {
        $this->params['customOrderExt'] = $customOrderExt->getInstance();
    }

    public function setThirdInfo($thirdInfo)
    {
        $this->params['thirdInfo'] = $thirdInfo->getInstance();
    }

    public function getCpin()
    {
        return $this->cpin;
    }

    public function setCpin($cpin)
    {
        $this->params['cpin'] = $cpin;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
