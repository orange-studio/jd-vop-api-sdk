<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class PaymentInfo
{
    private $params = array();
    private $paymentType;

    function __construct()
    {}

    public function getPaymentType()
    {
        return $this->paymentType;
    }

    public function setPaymentType($paymentType)
    {
        $this->params['paymentType'] = $paymentType;
    }

    public function setPaymentDetailList($paymentDetailList)
    {
        $size = count($paymentDetailList);
        for ($i = 0; $i < $size; $i++) {
            $paymentDetailList [$i] = $paymentDetailList [$i]->getInstance();
        }
        $this->params['paymentDetailList'] = $paymentDetailList;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
