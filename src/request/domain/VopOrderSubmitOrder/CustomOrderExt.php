<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class CustomOrderExt
{
    private $params = array();
    private $k2;
    private $v2;

    function __construct()
    {}

    public function getK2()
    {
        return $this->k2;
    }

    public function setK2($k2)
    {
        $this->params['k2'] = $k2;
    }

    public function getV2()
    {
        return $this->v2;
    }

    public function setV2($v2)
    {
        $this->params['v2'] = $v2;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
