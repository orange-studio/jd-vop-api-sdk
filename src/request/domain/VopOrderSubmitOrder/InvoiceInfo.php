<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class InvoiceInfo
{
    private $params = array();
    private $invoiceCompanyName;
    private $invoiceType;
    private $invoiceSelectedTitle;
    private $invoiceContentType;
    private $invoiceName;
    private $invoicePhone;
    private $invoiceProvinceId;
    private $invoiceCityId;
    private $invoiceCountyId;
    private $invoiceTownId;
    private $invoiceAddress;
    private $invoiceRegCompanyName;
    private $invoiceRegCode;
    private $invoiceRegAddress;
    private $invoiceRegPhone;
    private $invoiceRegBank;
    private $invoiceRegBankAccount;
    private $invoicePutType;

    function __construct()
    {}

    public function getInvoiceCompanyName()
    {
        return $this->invoiceCompanyName;
    }

    public function setInvoiceCompanyName($invoiceCompanyName)
    {
        $this->params['invoiceCompanyName'] = $invoiceCompanyName;
    }

    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    public function setInvoiceType($invoiceType)
    {
        $this->params['invoiceType'] = $invoiceType;
    }

    public function getInvoiceSelectedTitle()
    {
        return $this->invoiceSelectedTitle;
    }

    public function setInvoiceSelectedTitle($invoiceSelectedTitle)
    {
        $this->params['invoiceSelectedTitle'] = $invoiceSelectedTitle;
    }

    public function getInvoiceContentType()
    {
        return $this->invoiceContentType;
    }

    public function setInvoiceContentType($invoiceContentType)
    {
        $this->params['invoiceContentType'] = $invoiceContentType;
    }

    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    public function setInvoiceName($invoiceName)
    {
        $this->params['invoiceName'] = $invoiceName;
    }

    public function getInvoicePhone()
    {
        return $this->invoicePhone;
    }

    public function setInvoicePhone($invoicePhone)
    {
        $this->params['invoicePhone'] = $invoicePhone;
    }

    public function getInvoiceProvinceId()
    {
        return $this->invoiceProvinceId;
    }

    public function setInvoiceProvinceId($invoiceProvinceId)
    {
        $this->params['invoiceProvinceId'] = $invoiceProvinceId;
    }

    public function getInvoiceCityId()
    {
        return $this->invoiceCityId;
    }

    public function setInvoiceCityId($invoiceCityId)
    {
        $this->params['invoiceCityId'] = $invoiceCityId;
    }

    public function getInvoiceCountyId()
    {
        return $this->invoiceCountyId;
    }

    public function setInvoiceCountyId($invoiceCountyId)
    {
        $this->params['invoiceCountyId'] = $invoiceCountyId;
    }

    public function getInvoiceTownId()
    {
        return $this->invoiceTownId;
    }

    public function setInvoiceTownId($invoiceTownId)
    {
        $this->params['invoiceTownId'] = $invoiceTownId;
    }

    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    public function setInvoiceAddress($invoiceAddress)
    {
        $this->params['invoiceAddress'] = $invoiceAddress;
    }

    public function getInvoiceRegCompanyName()
    {
        return $this->invoiceRegCompanyName;
    }

    public function setInvoiceRegCompanyName($invoiceRegCompanyName)
    {
        $this->params['invoiceRegCompanyName'] = $invoiceRegCompanyName;
    }

    public function getInvoiceRegCode()
    {
        return $this->invoiceRegCode;
    }

    public function setInvoiceRegCode($invoiceRegCode)
    {
        $this->params['invoiceRegCode'] = $invoiceRegCode;
    }

    public function getInvoiceRegAddress()
    {
        return $this->invoiceRegAddress;
    }

    public function setInvoiceRegAddress($invoiceRegAddress)
    {
        $this->params['invoiceRegAddress'] = $invoiceRegAddress;
    }

    public function getInvoiceRegPhone()
    {
        return $this->invoiceRegPhone;
    }

    public function setInvoiceRegPhone($invoiceRegPhone)
    {
        $this->params['invoiceRegPhone'] = $invoiceRegPhone;
    }

    public function getInvoiceRegBank()
    {
        return $this->invoiceRegBank;
    }

    public function setInvoiceRegBank($invoiceRegBank)
    {
        $this->params['invoiceRegBank'] = $invoiceRegBank;
    }

    public function getInvoiceRegBankAccount()
    {
        return $this->invoiceRegBankAccount;
    }

    public function setInvoiceRegBankAccount($invoiceRegBankAccount)
    {
        $this->params['invoiceRegBankAccount'] = $invoiceRegBankAccount;
    }

    public function getInvoicePutType()
    {
        return $this->invoicePutType;
    }

    public function setInvoicePutType($invoicePutType)
    {
        $this->params['invoicePutType'] = $invoicePutType;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
