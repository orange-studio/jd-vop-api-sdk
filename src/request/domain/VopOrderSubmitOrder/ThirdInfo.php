<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class ThirdInfo
{
    private $params = array();
    private $thirdPurchaserAccount;
    private $thirdPurchaserName;
    private $thirdPurchaserPhone;

    function __construct()
    {}

    public function getThirdPurchaserAccount()
    {
        return $this->thirdPurchaserAccount;
    }

    public function setThirdPurchaserAccount($thirdPurchaserAccount)
    {
        $this->params['thirdPurchaserAccount'] = $thirdPurchaserAccount;
    }

    public function getThirdPurchaserName()
    {
        return $this->thirdPurchaserName;
    }

    public function setThirdPurchaserName($thirdPurchaserName)
    {
        $this->params['thirdPurchaserName'] = $thirdPurchaserName;
    }

    public function getThirdPurchaserPhone()
    {
        return $this->thirdPurchaserPhone;
    }

    public function setThirdPurchaserPhone($thirdPurchaserPhone)
    {
        $this->params['thirdPurchaserPhone'] = $thirdPurchaserPhone;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
