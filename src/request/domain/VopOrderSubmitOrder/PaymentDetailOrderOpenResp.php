<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class PaymentDetailOrderOpenResp
{
    private $params = array();
    private $payDetailMoney;
    private $payDetailType;
    private $payDetailFlag;

    function __construct()
    {}

    public function getPayDetailMoney()
    {
        return $this->payDetailMoney;
    }

    public function setPayDetailMoney($payDetailMoney)
    {
        $this->params['payDetailMoney'] = $payDetailMoney;
    }

    public function getPayDetailType()
    {
        return $this->payDetailType;
    }

    public function setPayDetailType($payDetailType)
    {
        $this->params['payDetailType'] = $payDetailType;
    }

    public function getPayDetailFlag()
    {
        return $this->payDetailFlag;
    }

    public function setPayDetailFlag($payDetailFlag)
    {
        $this->params['payDetailFlag'] = $payDetailFlag;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
