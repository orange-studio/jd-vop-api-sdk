<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class SkuInfo
{
    private $params = array();
    private $skuNeedGift;
    private $skuNum;
    private $skuId;
    private $skuUnitPrice;

    function __construct()
    {}

    public function getSkuNeedGift()
    {
        return $this->skuNeedGift;
    }

    public function setSkuNeedGift($skuNeedGift)
    {
        $this->params['skuNeedGift'] = $skuNeedGift;
    }

    public function getSkuNum()
    {
        return $this->skuNum;
    }

    public function setSkuNum($skuNum)
    {
        $this->params['skuNum'] = $skuNum;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->params['skuId'] = $skuId;
    }

    public function getSkuUnitPrice()
    {
        return $this->skuUnitPrice;
    }

    public function setSkuUnitPrice($skuUnitPrice)
    {
        $this->params['skuUnitPrice'] = $skuUnitPrice;
    }

    public function setYanbaoSkuInfoList($yanbaoSkuInfoList)
    {
        $size = count($yanbaoSkuInfoList);
        for ($i = 0; $i < $size; $i++) {
            $yanbaoSkuInfoList [$i] = $yanbaoSkuInfoList [$i]->getInstance();
        }
        $this->params['yanbaoSkuInfoList'] = $yanbaoSkuInfoList;
    }

    public function setCustomSkuExt($customSkuExt)
    {
        $this->params['customSkuExt'] = $customSkuExt->getInstance();
    }


    function getInstance(): array
    {
        return $this->params;
    }
}
