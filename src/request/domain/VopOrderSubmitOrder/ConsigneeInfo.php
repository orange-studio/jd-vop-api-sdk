<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class ConsigneeInfo
{
    private $params = array();
    private $consigneeName;
    private $consigneeProvinceId;
    private $consigneeCityId;
    private $consigneeCountyId;
    private $consigneeTownId;
    private $consigneeAddress;
    private $consigneeZip;
    private $consigneeMobile;
    private $consigneeEmail;
    private $consigneePhone;

    function __construct()
    {}

    public function getConsigneeName()
    {
        return $this->consigneeName;
    }

    public function setConsigneeName($consigneeName)
    {
        $this->params['consigneeName'] = $consigneeName;
    }

    public function getConsigneeProvinceId()
    {
        return $this->consigneeProvinceId;
    }

    public function setConsigneeProvinceId($consigneeProvinceId)
    {
        $this->params['consigneeProvinceId'] = $consigneeProvinceId;
    }

    public function getConsigneeCityId()
    {
        return $this->consigneeCityId;
    }

    public function setConsigneeCityId($consigneeCityId)
    {
        $this->params['consigneeCityId'] = $consigneeCityId;
    }

    public function getConsigneeCountyId()
    {
        return $this->consigneeCountyId;
    }

    public function setConsigneeCountyId($consigneeCountyId)
    {
        $this->params['consigneeCountyId'] = $consigneeCountyId;
    }

    public function getConsigneeTownId()
    {
        return $this->consigneeTownId;
    }

    public function setConsigneeTownId($consigneeTownId)
    {
        $this->params['consigneeTownId'] = $consigneeTownId;
    }

    public function getConsigneeAddress()
    {
        return $this->consigneeAddress;
    }

    public function setConsigneeAddress($consigneeAddress)
    {
        $this->params['consigneeAddress'] = $consigneeAddress;
    }

    public function getConsigneeZip()
    {
        return $this->consigneeZip;
    }

    public function setConsigneeZip($consigneeZip)
    {
        $this->params['consigneeZip'] = $consigneeZip;
    }

    public function getConsigneeMobile()
    {
        return $this->consigneeMobile;
    }

    public function setConsigneeMobile($consigneeMobile)
    {
        $this->params['consigneeMobile'] = $consigneeMobile;
    }

    public function getConsigneeEmail()
    {
        return $this->consigneeEmail;
    }

    public function setConsigneeEmail($consigneeEmail)
    {
        $this->params['consigneeEmail'] = $consigneeEmail;
    }

    public function getConsigneePhone()
    {
        return $this->consigneePhone;
    }

    public function setConsigneePhone($consigneePhone)
    {
        $this->params['consigneePhone'] = $consigneePhone;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
