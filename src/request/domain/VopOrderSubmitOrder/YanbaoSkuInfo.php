<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class YanbaoSkuInfo
{
    private $params = array();
    private $yanbaoSkuNum;
    private $yanbaoSkuId;
    private $yanbaoSkuUnitPrice;
    private $yanbaoSkuNeedGift;

    function __construct()
    {}

    public function getYanbaoSkuNum()
    {
        return $this->yanbaoSkuNum;
    }

    public function setYanbaoSkuNum($yanbaoSkuNum)
    {
        $this->params['yanbaoSkuNum'] = $yanbaoSkuNum;
    }

    public function getYanbaoSkuId()
    {
        return $this->yanbaoSkuId;
    }

    public function setYanbaoSkuId($yanbaoSkuId)
    {
        $this->params['yanbaoSkuId'] = $yanbaoSkuId;
    }

    public function getYanbaoSkuUnitPrice()
    {
        return $this->yanbaoSkuUnitPrice;
    }

    public function setYanbaoSkuUnitPrice($yanbaoSkuUnitPrice)
    {
        $this->params['yanbaoSkuUnitPrice'] = $yanbaoSkuUnitPrice;
    }

    public function getYanbaoSkuNeedGift()
    {
        return $this->yanbaoSkuNeedGift;
    }

    public function setYanbaoSkuNeedGift($yanbaoSkuNeedGift)
    {
        $this->params['yanbaoSkuNeedGift'] = $yanbaoSkuNeedGift;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
