<?php

namespace JdVopClient\request\domain\VopOrderSubmitOrder;

class CustomSkuExt
{
    private $params = array();
    private $k1;
    private $v1;

    function __construct()
    {}

    public function getK1()
    {
        return $this->k1;
    }

    public function setK1($k1)
    {
        $this->params['k1'] = $k1;
    }

    public function getV1()
    {
        return $this->v1;
    }

    public function setV1($v1)
    {
        $this->params['v1'] = $v1;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
