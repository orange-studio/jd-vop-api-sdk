<?php

namespace JdVopClient\request\domain\VopOrderUpdateCustomOrderExt;

class RpcReq
{
    private $params = array();
    private $josRemoteIp;
    private $pin;
    private $sourceType;
    private $appKey;

    function __construct()
    {}

    public function getJosRemoteIp()
    {
        return $this->josRemoteIp;
    }

    public function setJosRemoteIp($josRemoteIp)
    {
        $this->params['josRemoteIp'] = $josRemoteIp;
    }

    public function getPin()
    {
        return $this->pin;
    }

    public function setPin($pin)
    {
        $this->params['pin'] = $pin;
    }

    public function getSourceType()
    {
        return $this->sourceType;
    }

    public function setSourceType($sourceType)
    {
        $this->params['sourceType'] = $sourceType;
    }

    public function setOrderExtInfoList($orderExtInfoList)
    {
        $size = count($orderExtInfoList);
        for ($i = 0; $i < $size; $i++) {
            $orderExtInfoList [$i] = $orderExtInfoList [$i]->getInstance();
        }
        $this->params['orderExtInfoList'] = $orderExtInfoList;
    }

    public function getAppKey()
    {
        return $this->appKey;
    }

    public function setAppKey($appKey)
    {
        $this->params['appKey'] = $appKey;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
