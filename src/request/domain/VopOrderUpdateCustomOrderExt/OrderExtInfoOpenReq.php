<?php

namespace JdVopClient\request\domain\VopOrderUpdateCustomOrderExt;

class OrderExtInfoOpenReq
{
    private $params = array();
    private $value;
    private $jdOrderId;
    private $skuId;
    private $key;

    function __construct()
    {}

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->params['value'] = $value;
    }

    public function getJdOrderId()
    {
        return $this->jdOrderId;
    }

    public function setJdOrderId($jdOrderId)
    {
        $this->params['jdOrderId'] = $jdOrderId;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->params['skuId'] = $skuId;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function setKey($key)
    {
        $this->params['key'] = $key;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
