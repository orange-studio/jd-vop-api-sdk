<?php

namespace JdVopClient\request\domain\VopGoodsApplyReserve;

class NoStockList
{
    private $params = array();
    private $applyId;
    private $price;
    private $num;
    private $skuId;

    function __construct()
    {}

    public function getApplyId()
    {
        return $this->applyId;
    }

    public function setApplyId($applyId)
    {
        $this->params['applyId'] = $applyId;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->params['price'] = $price;
    }

    public function getNum()
    {
        return $this->num;
    }

    public function setNum($num)
    {
        $this->params['num'] = $num;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->params['skuId'] = $skuId;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
