<?php

namespace JdVopClient\request\domain\VopGoodsApplyReserve;

class ReserveInfo
{
    private $params = array();
    private $exDeliveryDate;
    private $applicantQQ;
    private $address;
    private $josRemoteIp;
    private $reserveId;
    private $applyReason;
    private $applicantContact;
    private $applicant;
    private $pin;
    private $sourceType;
    private $appKey;

    function __construct()
    {}

    public function getExDeliveryDate()
    {
        return $this->exDeliveryDate;
    }

    public function setExDeliveryDate($exDeliveryDate)
    {
        $this->params['exDeliveryDate'] = $exDeliveryDate;
    }

    public function getApplicantQQ()
    {
        return $this->applicantQQ;
    }

    public function setApplicantQQ($applicantQQ)
    {
        $this->params['applicantQQ'] = $applicantQQ;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->params['address'] = $address;
    }

    public function getJosRemoteIp()
    {
        return $this->josRemoteIp;
    }

    public function setJosRemoteIp($josRemoteIp)
    {
        $this->params['josRemoteIp'] = $josRemoteIp;
    }

    public function getReserveId()
    {
        return $this->reserveId;
    }

    public function setReserveId($reserveId)
    {
        $this->params['reserveId'] = $reserveId;
    }

    public function getApplyReason()
    {
        return $this->applyReason;
    }

    public function setApplyReason($applyReason)
    {
        $this->params['applyReason'] = $applyReason;
    }

    public function getApplicantContact()
    {
        return $this->applicantContact;
    }

    public function setApplicantContact($applicantContact)
    {
        $this->params['applicantContact'] = $applicantContact;
    }

    public function setReserveSkus($reserveSkus)
    {
        $size = count($reserveSkus);
        for ($i = 0; $i < $size; $i++) {
            $reserveSkus [$i] = $reserveSkus [$i]->getInstance();
        }
        $this->params['reserveSkus'] = $reserveSkus;
    }

    public function getApplicant()
    {
        return $this->applicant;
    }

    public function setApplicant($applicant)
    {
        $this->params['applicant'] = $applicant;
    }

    public function getPin()
    {
        return $this->pin;
    }

    public function setPin($pin)
    {
        $this->params['pin'] = $pin;
    }

    public function getSourceType()
    {
        return $this->sourceType;
    }

    public function setSourceType($sourceType)
    {
        $this->params['sourceType'] = $sourceType;
    }

    public function getAppKey()
    {
        return $this->appKey;
    }

    public function setAppKey($appKey)
    {
        $this->params['appKey'] = $appKey;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
