<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class CustomerInfoVo
{
    private $params = array();
    private $customerName;
    private $customerTel;
    private $customerMobilePhone;
    private $customerEmail;
    private $customerPostcode;
    private $customerContactName;

    function __construct()
    {}

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function setCustomerName($customerName)
    {
        $this->params['customerName'] = $customerName;
    }

    public function getCustomerTel()
    {
        return $this->customerTel;
    }

    public function setCustomerTel($customerTel)
    {
        $this->params['customerTel'] = $customerTel;
    }

    public function getCustomerMobilePhone()
    {
        return $this->customerMobilePhone;
    }

    public function setCustomerMobilePhone($customerMobilePhone)
    {
        $this->params['customerMobilePhone'] = $customerMobilePhone;
    }

    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail($customerEmail)
    {
        $this->params['customerEmail'] = $customerEmail;
    }

    public function getCustomerPostcode()
    {
        return $this->customerPostcode;
    }

    public function setCustomerPostcode($customerPostcode)
    {
        $this->params['customerPostcode'] = $customerPostcode;
    }

    public function getCustomerContactName()
    {
        return $this->customerContactName;
    }

    public function setCustomerContactName($customerContactName)
    {
        $this->params['customerContactName'] = $customerContactName;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
