<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class PickupWareInfoOpenReq
{
    private $params = array();
    private $pickWareType;
    private $pickWareProvince;
    private $pickWareCounty;
    private $pickWareCity;
    private $pickWareVillage;
    private $pickWareAddress;
    private $reserveDateBegin;
    private $reserveDateEnd;

    function __construct()
    {}

    public function getPickWareType()
    {
        return $this->pickWareType;
    }

    public function setPickWareType($pickWareType)
    {
        $this->params['pickWareType'] = $pickWareType;
    }

    public function getPickWareProvince()
    {
        return $this->pickWareProvince;
    }

    public function setPickWareProvince($pickWareProvince)
    {
        $this->params['pickWareProvince'] = $pickWareProvince;
    }

    public function getPickWareCounty()
    {
        return $this->pickWareCounty;
    }

    public function setPickWareCounty($pickWareCounty)
    {
        $this->params['pickWareCounty'] = $pickWareCounty;
    }

    public function getPickWareCity()
    {
        return $this->pickWareCity;
    }

    public function setPickWareCity($pickWareCity)
    {
        $this->params['pickWareCity'] = $pickWareCity;
    }

    public function getPickWareVillage()
    {
        return $this->pickWareVillage;
    }

    public function setPickWareVillage($pickWareVillage)
    {
        $this->params['pickWareVillage'] = $pickWareVillage;
    }

    public function getPickWareAddress()
    {
        return $this->pickWareAddress;
    }

    public function setPickWareAddress($pickWareAddress)
    {
        $this->params['pickWareAddress'] = $pickWareAddress;
    }

    public function getReserveDateBegin()
    {
        return $this->reserveDateBegin;
    }

    public function setReserveDateBegin($reserveDateBegin)
    {
        $this->params['reserveDateBegin'] = $reserveDateBegin;
    }

    public function getReserveDateEnd()
    {
        return $this->reserveDateEnd;
    }

    public function setReserveDateEnd($reserveDateEnd)
    {
        $this->params['reserveDateEnd'] = $reserveDateEnd;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
