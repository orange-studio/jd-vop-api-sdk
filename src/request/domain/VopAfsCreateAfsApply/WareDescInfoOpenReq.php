<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class WareDescInfoOpenReq
{
    private $params = array();
    private $isNeedDetectionReport;
    private $lossPreventionTagFlag;
    private $isHasPackage;
    private $questionDesc;
    private $packageDesc;
    private $questionPic;

    function __construct()
    {}

    public function getIsNeedDetectionReport()
    {
        return $this->isNeedDetectionReport;
    }

    public function setIsNeedDetectionReport($isNeedDetectionReport)
    {
        $this->params['isNeedDetectionReport'] = $isNeedDetectionReport;
    }

    public function getLossPreventionTagFlag()
    {
        return $this->lossPreventionTagFlag;
    }

    public function setLossPreventionTagFlag($lossPreventionTagFlag)
    {
        $this->params['lossPreventionTagFlag'] = $lossPreventionTagFlag;
    }

    public function getIsHasPackage()
    {
        return $this->isHasPackage;
    }

    public function setIsHasPackage($isHasPackage)
    {
        $this->params['isHasPackage'] = $isHasPackage;
    }

    public function getQuestionDesc()
    {
        return $this->questionDesc;
    }

    public function setQuestionDesc($questionDesc)
    {
        $this->params['questionDesc'] = $questionDesc;
    }

    public function getPackageDesc()
    {
        return $this->packageDesc;
    }

    public function setPackageDesc($packageDesc)
    {
        $this->params['packageDesc'] = $packageDesc;
    }

    public function getQuestionPic()
    {
        return $this->questionPic;
    }

    public function setQuestionPic($questionPic)
    {
        $this->params['questionPic'] = $questionPic;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
