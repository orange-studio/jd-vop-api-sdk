<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class ApplyAfterSaleOpenReq
{
    private $params = array();
    private $thirdApplyId;
    private $isHasInvoice;
    private $orderId;

    function __construct()
    {}

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->params['thirdApplyId'] = $thirdApplyId;
    }

    public function getIsHasInvoice()
    {
        return $this->isHasInvoice;
    }

    public function setIsHasInvoice($isHasInvoice)
    {
        $this->params['isHasInvoice'] = $isHasInvoice;
    }

    public function setApplyInfoItemOpenReqList($applyInfoItemOpenReqList)
    {
        $size = count($applyInfoItemOpenReqList);
        for ($i = 0; $i < $size; $i++) {
            $applyInfoItemOpenReqList [$i] = $applyInfoItemOpenReqList [$i]->getInstance();
        }
        $this->params['applyInfoItemOpenReqList'] = $applyInfoItemOpenReqList;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->params['orderId'] = $orderId;
    }

    public function setCustomerInfoVo($customerInfoVo)
    {
        $this->params['customerInfoVo'] = $customerInfoVo->getInstance();
    }

    public function setPickupWareInfoOpenReq($pickupWareInfoOpenReq)
    {
        $this->params['pickupWareInfoOpenReq'] = $pickupWareInfoOpenReq->getInstance();
    }

    public function setReturnWareInfoOpenReq($returnWareInfoOpenReq)
    {
        $this->params['returnWareInfoOpenReq'] = $returnWareInfoOpenReq->getInstance();
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
