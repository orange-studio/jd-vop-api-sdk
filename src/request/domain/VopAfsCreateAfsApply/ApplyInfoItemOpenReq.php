<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class ApplyInfoItemOpenReq
{
    private $params = array();
    private $customerExpect;

    function __construct()
    {}

    public function getCustomerExpect()
    {
        return $this->customerExpect;
    }

    public function setCustomerExpect($customerExpect)
    {
        $this->params['customerExpect'] = $customerExpect;
    }

    public function setWareDescInfoOpenReq($wareDescInfoOpenReq)
    {
        $this->params['wareDescInfoOpenReq'] = $wareDescInfoOpenReq->getInstance();
    }

    public function setWareDetailInfoOpenReq($wareDetailInfoOpenReq)
    {
        $this->params['wareDetailInfoOpenReq'] = $wareDetailInfoOpenReq->getInstance();
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
