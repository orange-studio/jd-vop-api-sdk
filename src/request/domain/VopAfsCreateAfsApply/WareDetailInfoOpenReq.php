<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class WareDetailInfoOpenReq
{
    private $params = array();
    private $wareId;
    private $mainWareId;
    private $wareName;
    private $wareNum;
    private $wareDescribe;
    private $payPrice;
    private $wareType;

    function __construct()
    {}

    public function getWareId()
    {
        return $this->wareId;
    }

    public function setWareId($wareId)
    {
        $this->params['wareId'] = $wareId;
    }

    public function getMainWareId()
    {
        return $this->mainWareId;
    }

    public function setMainWareId($mainWareId)
    {
        $this->params['mainWareId'] = $mainWareId;
    }

    public function getWareName()
    {
        return $this->wareName;
    }

    public function setWareName($wareName)
    {
        $this->params['wareName'] = $wareName;
    }

    public function getWareNum()
    {
        return $this->wareNum;
    }

    public function setWareNum($wareNum)
    {
        $this->params['wareNum'] = $wareNum;
    }

    public function getWareDescribe()
    {
        return $this->wareDescribe;
    }

    public function setWareDescribe($wareDescribe)
    {
        $this->params['wareDescribe'] = $wareDescribe;
    }

    public function getPayPrice()
    {
        return $this->payPrice;
    }

    public function setPayPrice($payPrice)
    {
        $this->params['payPrice'] = $payPrice;
    }

    public function getWareType()
    {
        return $this->wareType;
    }

    public function setWareType($wareType)
    {
        $this->params['wareType'] = $wareType;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
