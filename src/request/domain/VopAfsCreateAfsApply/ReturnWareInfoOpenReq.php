<?php

namespace JdVopClient\request\domain\VopAfsCreateAfsApply;

class ReturnWareInfoOpenReq
{
    private $params = array();
    private $returnWareType;
    private $returnWareProvince;
    private $returnWareCity;
    private $returnWareCountry;
    private $returnWareVillage;
    private $returnWareAddress;

    function __construct()
    {}

    public function getReturnWareType()
    {
        return $this->returnWareType;
    }

    public function setReturnWareType($returnWareType)
    {
        $this->params['returnWareType'] = $returnWareType;
    }

    public function getReturnWareProvince()
    {
        return $this->returnWareProvince;
    }

    public function setReturnWareProvince($returnWareProvince)
    {
        $this->params['returnWareProvince'] = $returnWareProvince;
    }

    public function getReturnWareCity()
    {
        return $this->returnWareCity;
    }

    public function setReturnWareCity($returnWareCity)
    {
        $this->params['returnWareCity'] = $returnWareCity;
    }

    public function getReturnWareCountry()
    {
        return $this->returnWareCountry;
    }

    public function setReturnWareCountry($returnWareCountry)
    {
        $this->params['returnWareCountry'] = $returnWareCountry;
    }

    public function getReturnWareVillage()
    {
        return $this->returnWareVillage;
    }

    public function setReturnWareVillage($returnWareVillage)
    {
        $this->params['returnWareVillage'] = $returnWareVillage;
    }

    public function getReturnWareAddress()
    {
        return $this->returnWareAddress;
    }

    public function setReturnWareAddress($returnWareAddress)
    {
        $this->params['returnWareAddress'] = $returnWareAddress;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
