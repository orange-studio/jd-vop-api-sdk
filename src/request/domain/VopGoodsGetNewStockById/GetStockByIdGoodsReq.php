<?php

namespace JdVopClient\request\domain\VopGoodsGetNewStockById;

class GetStockByIdGoodsReq
{
    private $params = array();

    function __construct()
    {}

    public function setSkuNumInfoList($skuNumInfoList)
    {
        $size = count($skuNumInfoList);
        for ($i = 0; $i < $size; $i++) {
            $skuNumInfoList [$i] = $skuNumInfoList [$i]->getInstance();
        }
        $this->params['skuNumInfoList'] = $skuNumInfoList;
    }

    public function setAreaInfo($areaInfo)
    {
        $this->params['areaInfo'] = $areaInfo->getInstance();
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
