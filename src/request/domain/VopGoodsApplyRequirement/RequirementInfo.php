<?php

namespace JdVopClient\request\domain\VopGoodsApplyRequirement;

class RequirementInfo
{
    private $params = array();
    private $applicantQQ;
    private $address;
    private $josRemoteIp;
    private $applyReason;
    private $applicantContact;
    private $applicant;
    private $pin;
    private $sourceType;
    private $requirementId;
    private $appKey;

    function __construct()
    {}

    public function getApplicantQQ()
    {
        return $this->applicantQQ;
    }

    public function setApplicantQQ($applicantQQ)
    {
        $this->params['applicantQQ'] = $applicantQQ;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->params['address'] = $address;
    }

    public function getJosRemoteIp()
    {
        return $this->josRemoteIp;
    }

    public function setJosRemoteIp($josRemoteIp)
    {
        $this->params['josRemoteIp'] = $josRemoteIp;
    }

    public function getApplyReason()
    {
        return $this->applyReason;
    }

    public function setApplyReason($applyReason)
    {
        $this->params['applyReason'] = $applyReason;
    }

    public function getApplicantContact()
    {
        return $this->applicantContact;
    }

    public function setApplicantContact($applicantContact)
    {
        $this->params['applicantContact'] = $applicantContact;
    }

    public function setRequirementSkus($requirementSkus)
    {
        $size = count($requirementSkus);
        for ($i = 0; $i < $size; $i++) {
            $requirementSkus [$i] = $requirementSkus [$i]->getInstance();
        }
        $this->params['requirementSkus'] = $requirementSkus;
    }

    public function getApplicant()
    {
        return $this->applicant;
    }

    public function setApplicant($applicant)
    {
        $this->params['applicant'] = $applicant;
    }

    public function getPin()
    {
        return $this->pin;
    }

    public function setPin($pin)
    {
        $this->params['pin'] = $pin;
    }

    public function getSourceType()
    {
        return $this->sourceType;
    }

    public function setSourceType($sourceType)
    {
        $this->params['sourceType'] = $sourceType;
    }

    public function getRequirementId()
    {
        return $this->requirementId;
    }

    public function setRequirementId($requirementId)
    {
        $this->params['requirementId'] = $requirementId;
    }

    public function getAppKey()
    {
        return $this->appKey;
    }

    public function setAppKey($appKey)
    {
        $this->params['appKey'] = $appKey;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
