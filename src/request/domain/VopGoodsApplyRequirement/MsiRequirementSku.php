<?php

namespace JdVopClient\request\domain\VopGoodsApplyRequirement;

class MsiRequirementSku
{
    private $params = array();
    private $applyId;
    private $productCode;
    private $productBrand;
    private $prdModel;
    private $description;
    private $packaging;
    private $source;
    private $saleUnit;
    private $productName;
    private $productCategory;
    private $skuLink;

    function __construct()
    {}

    public function getApplyId()
    {
        return $this->applyId;
    }

    public function setApplyId($applyId)
    {
        $this->params['applyId'] = $applyId;
    }

    public function getProductCode()
    {
        return $this->productCode;
    }

    public function setProductCode($productCode)
    {
        $this->params['productCode'] = $productCode;
    }

    public function getProductBrand()
    {
        return $this->productBrand;
    }

    public function setProductBrand($productBrand)
    {
        $this->params['productBrand'] = $productBrand;
    }

    public function getPrdModel()
    {
        return $this->prdModel;
    }

    public function setPrdModel($prdModel)
    {
        $this->params['prdModel'] = $prdModel;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->params['description'] = $description;
    }

    public function getPackaging()
    {
        return $this->packaging;
    }

    public function setPackaging($packaging)
    {
        $this->params['packaging'] = $packaging;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source)
    {
        $this->params['source'] = $source;
    }

    public function getSaleUnit()
    {
        return $this->saleUnit;
    }

    public function setSaleUnit($saleUnit)
    {
        $this->params['saleUnit'] = $saleUnit;
    }

    public function getProductName()
    {
        return $this->productName;
    }

    public function setProductName($productName)
    {
        $this->params['productName'] = $productName;
    }

    public function getProductCategory()
    {
        return $this->productCategory;
    }

    public function setProductCategory($productCategory)
    {
        $this->params['productCategory'] = $productCategory;
    }

    public function getSkuLink()
    {
        return $this->skuLink;
    }

    public function setSkuLink($skuLink)
    {
        $this->params['skuLink'] = $skuLink;
    }

    function getInstance(): array
    {
        return $this->params;
    }
}
