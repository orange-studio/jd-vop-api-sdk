<?php

namespace JdVopClient\request;

class VopOrderUpdateCustomOrderExtRequest
{
    private $apiParas = array();
    private $version;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.updateCustomOrderExt";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function setRpcReq($rpcReq)
    {
        $this->apiParas['rpcReq'] = $rpcReq;
    }

    public function getRpcReq()
    {
        return $this->apiParas['rpcReq'];
    }
}
