<?php

namespace JdVopClient\request;

class VopMessageQueryTransByVopNormalRequest
{
    private $apiParas = array();
    private $version;
    private $type;
    private $readType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.message.queryTransByVopNormal";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type             = $type;
        $this->apiParas["type"] = $type;
    }

    public function getReadType()
    {
        return $this->readType;
    }

    public function setReadType($readType)
    {
        $this->readType             = $readType;
        $this->apiParas["readType"] = $readType;
    }
}

 
