<?php

namespace JdVopClient\request;

class VopOrderGetOrderReceiptUrlRequest
{
    private $apiParas = array();
    private $version;
    private $orderId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.getOrderReceiptUrl";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId             = $orderId;
        $this->apiParas["orderId"] = $orderId;
    }
}
        
 
