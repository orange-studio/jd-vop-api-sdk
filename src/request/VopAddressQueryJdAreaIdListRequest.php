<?php

namespace JdVopClient\request;

class VopAddressQueryJdAreaIdListRequest
{
    private $apiParas = array();
    private $version;
    private $areaLevel;
    private $jdAreaId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.address.queryJdAreaIdList";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getAreaLevel()
    {
        return $this->areaLevel;
    }

    public function setAreaLevel($areaLevel)
    {
        $this->areaLevel             = $areaLevel;
        $this->apiParas["areaLevel"] = $areaLevel;
    }

    public function getJdAreaId()
    {
        return $this->jdAreaId;
    }

    public function setJdAreaId($jdAreaId)
    {
        $this->jdAreaId             = $jdAreaId;
        $this->apiParas["jdAreaId"] = $jdAreaId;
    }
}
        
 
