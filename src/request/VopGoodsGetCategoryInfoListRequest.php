<?php

namespace JdVopClient\request;

class VopGoodsGetCategoryInfoListRequest
{
    private $apiParas = array();
    private $version;
    private $categoryId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.getCategoryInfoList";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId)
    {
        $this->categoryId             = $categoryId;
        $this->apiParas["categoryId"] = $categoryId;
    }
}
        
 
