<?php

namespace JdVopClient\request;

class VopInvoiceQueryElectronicInvoiceDetailRequest
{
    private $apiParas = array();
    private $version;
    private $jdOrderId;
    private $ivcType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.invoice.queryElectronicInvoiceDetail";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getJdOrderId()
    {
        return $this->jdOrderId;
    }

    public function setJdOrderId($jdOrderId)
    {
        $this->jdOrderId             = $jdOrderId;
        $this->apiParas["jdOrderId"] = $jdOrderId;
    }

    public function getIvcType()
    {
        return $this->ivcType;
    }

    public function setIvcType($ivcType)
    {
        $this->ivcType             = $ivcType;
        $this->apiParas["ivcType"] = $ivcType;
    }
}
        
 
