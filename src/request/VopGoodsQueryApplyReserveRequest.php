<?php

namespace JdVopClient\request;

class VopGoodsQueryApplyReserveRequest
{
    private $apiParas = array();
    private $version;
    private $reserveId;
    private $jdReserveId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.queryApplyReserve";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getReserveId()
    {
        return $this->reserveId;
    }

    public function setReserveId($reserveId)
    {
        $this->reserveId             = $reserveId;
        $this->apiParas["reserveId"] = $reserveId;
    }

    public function getJdReserveId()
    {
        return $this->jdReserveId;
    }

    public function setJdReserveId($jdReserveId)
    {
        $this->jdReserveId             = $jdReserveId;
        $this->apiParas["jdReserveId"] = $jdReserveId;
    }
}
        
 
