<?php

namespace JdVopClient\request;

class VopGoodsQueryApplyRequirementRequest
{
    private $apiParas = array();
    private $version;
    private $jdRequirementId;
    private $requirementId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.queryApplyRequirement";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getJdRequirementId()
    {
        return $this->jdRequirementId;
    }

    public function setJdRequirementId($jdRequirementId)
    {
        $this->jdRequirementId             = $jdRequirementId;
        $this->apiParas["jdRequirementId"] = $jdRequirementId;
    }

    public function getRequirementId()
    {
        return $this->requirementId;
    }

    public function setRequirementId($requirementId)
    {
        $this->requirementId             = $requirementId;
        $this->apiParas["requirementId"] = $requirementId;
    }
}
        
 
