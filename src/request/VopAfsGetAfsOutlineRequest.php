<?php

namespace JdVopClient\request;

class VopAfsGetAfsOutlineRequest
{
    private $apiParas = array();
    private $version;
    private $thirdApplyId;
    private $pageSize;
    private $wareId;
    private $orderId;
    private $pageIndex;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.afs.getAfsOutline";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->thirdApplyId             = $thirdApplyId;
        $this->apiParas["thirdApplyId"] = $thirdApplyId;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }

    public function getWareId()
    {
        return $this->wareId;
    }

    public function setWareId($wareId)
    {
        $this->wareId             = $wareId;
        $this->apiParas["wareId"] = $wareId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId             = $orderId;
        $this->apiParas["orderId"] = $orderId;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex             = $pageIndex;
        $this->apiParas["pageIndex"] = $pageIndex;
    }
}
        
 
