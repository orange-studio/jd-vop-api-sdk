<?php

namespace JdVopClient\request;

class VopAfsQueryAfsAddressInfosRequest
{
    private $apiParas = array();
    private $version;
    private $thirdApplyId;
    private $orderId;
    private $customerPin;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.afs.queryAfsAddressInfos";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->thirdApplyId             = $thirdApplyId;
        $this->apiParas["thirdApplyId"] = $thirdApplyId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId             = $orderId;
        $this->apiParas["orderId"] = $orderId;
    }

    public function getCustomerPin()
    {
        return $this->customerPin;
    }

    public function setCustomerPin($customerPin)
    {
        $this->customerPin             = $customerPin;
        $this->apiParas["customerPin"] = $customerPin;
    }
}
        
 
