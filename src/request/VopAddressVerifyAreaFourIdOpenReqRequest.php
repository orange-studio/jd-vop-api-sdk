<?php

namespace JdVopClient\request;

class VopAddressVerifyAreaFourIdOpenReqRequest
{
    private $apiParas = array();
    private $version;
    private $provinceId;
    private $cityId;
    private $townId;
    private $countyId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.address.verifyAreaFourIdOpenReq";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getProvinceId()
    {
        return $this->provinceId;
    }

    public function setProvinceId($provinceId)
    {
        $this->provinceId             = $provinceId;
        $this->apiParas["provinceId"] = $provinceId;
    }

    public function getCityId()
    {
        return $this->cityId;
    }

    public function setCityId($cityId)
    {
        $this->cityId             = $cityId;
        $this->apiParas["cityId"] = $cityId;
    }

    public function getTownId()
    {
        return $this->townId;
    }

    public function setTownId($townId)
    {
        $this->townId             = $townId;
        $this->apiParas["townId"] = $townId;
    }

    public function getCountyId()
    {
        return $this->countyId;
    }

    public function setCountyId($countyId)
    {
        $this->countyId             = $countyId;
        $this->apiParas["countyId"] = $countyId;
    }
}
        
 
