<?php

namespace JdVopClient\request;

class VopOrderQueryDeliveryInfoRequest
{
    private $apiParas = array();
    private $version;
    private $thirdOrderId;
    private $jdOrderId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.queryDeliveryInfo";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getThirdOrderId()
    {
        return $this->thirdOrderId;
    }

    public function setThirdOrderId($thirdOrderId)
    {
        $this->thirdOrderId             = $thirdOrderId;
        $this->apiParas["thirdOrderId"] = $thirdOrderId;
    }

    public function getJdOrderId()
    {
        return $this->jdOrderId;
    }

    public function setJdOrderId($jdOrderId)
    {
        $this->jdOrderId             = $jdOrderId;
        $this->apiParas["jdOrderId"] = $jdOrderId;
    }
}
        
 
