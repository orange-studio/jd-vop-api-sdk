<?php

namespace JdVopClient\request;

class VopOrderPredictSkuPromiseRequest
{
    private $apiParas = array();
    private $version;
    private $skuNum;
    private $skuId;
    private $provinceId;
    private $cityId;
    private $countyId;
    private $townId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.predictSkuPromise";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getSkuNum()
    {
        return $this->skuNum;
    }

    public function setSkuNum($skuNum)
    {
        $this->skuNum             = $skuNum;
        $this->apiParas["skuNum"] = $skuNum;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->skuId             = $skuId;
        $this->apiParas["skuId"] = $skuId;
    }

    public function getProvinceId()
    {
        return $this->provinceId;
    }

    public function setProvinceId($provinceId)
    {
        $this->provinceId             = $provinceId;
        $this->apiParas["provinceId"] = $provinceId;
    }

    public function getCityId()
    {
        return $this->cityId;
    }

    public function setCityId($cityId)
    {
        $this->cityId             = $cityId;
        $this->apiParas["cityId"] = $cityId;
    }

    public function getCountyId()
    {
        return $this->countyId;
    }

    public function setCountyId($countyId)
    {
        $this->countyId             = $countyId;
        $this->apiParas["countyId"] = $countyId;
    }

    public function getTownId()
    {
        return $this->townId;
    }

    public function setTownId($townId)
    {
        $this->townId             = $townId;
        $this->apiParas["townId"] = $townId;
    }
}
        
 
