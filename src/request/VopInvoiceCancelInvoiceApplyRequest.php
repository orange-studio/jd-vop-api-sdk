<?php

namespace JdVopClient\request;

class VopInvoiceCancelInvoiceApplyRequest
{
    private $apiParas = array();
    private $version;
    private $markId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.invoice.cancelInvoiceApply";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getMarkId()
    {
        return $this->markId;
    }

    public function setMarkId($markId)
    {
        $this->markId             = $markId;
        $this->apiParas["markId"] = $markId;
    }
}
        
 
