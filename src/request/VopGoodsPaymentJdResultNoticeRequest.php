<?php

namespace JdVopClient\request;

class VopGoodsPaymentJdResultNoticeRequest
{
    private $apiParas = array();
    private $version;
    private $outSettleNo;
    private $bankChannelId;
    private $bankJoinCode;
    private $accountName;
    private $accountNo;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.paymentJdResultNotice";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getOutSettleNo()
    {
        return $this->outSettleNo;
    }

    public function setOutSettleNo($outSettleNo)
    {
        $this->outSettleNo             = $outSettleNo;
        $this->apiParas["outSettleNo"] = $outSettleNo;
    }

    public function getBankChannelId()
    {
        return $this->bankChannelId;
    }

    public function setBankChannelId($bankChannelId)
    {
        $this->bankChannelId             = $bankChannelId;
        $this->apiParas["bankChannelId"] = $bankChannelId;
    }

    public function getBankJoinCode()
    {
        return $this->bankJoinCode;
    }

    public function setBankJoinCode($bankJoinCode)
    {
        $this->bankJoinCode             = $bankJoinCode;
        $this->apiParas["bankJoinCode"] = $bankJoinCode;
    }

    public function getAccountName()
    {
        return $this->accountName;
    }

    public function setAccountName($accountName)
    {
        $this->accountName             = $accountName;
        $this->apiParas["accountName"] = $accountName;
    }

    public function getAccountNo()
    {
        return $this->accountNo;
    }

    public function setAccountNo($accountNo)
    {
        $this->accountNo             = $accountNo;
        $this->apiParas["accountNo"] = $accountNo;
    }
}
        
 
