<?php

namespace JdVopClient\request;

class VopInvoiceSubmitInvoiceApplyRequest
{
    private $apiParas = array();
    private $version;
    private $invoiceDate;
    private $markId;
    private $invoiceOrg;
    private $totalBatchInvoiceAmount;
    private $billToContact;
    private $title;
    private $currentBatch;
    private $enterpriseTaxpayer;
    private $billingType;
    private $settlementId;
    private $invoiceNum;
    private $enterpriseRegAddress;
    private $billToProvince;
    private $isMerge;
    private $enterpriseBankName;
    private $billToCounty;
    private $enterpriseBankAccount;
    private $invoiceType;
    private $billToTown;
    private $billToAddress;
    private $poNo;
    private $invoicePrice;
    private $supplierOrder;
    private $repaymentDate;
    private $invoiceRemark;
    private $totalBatch;
    private $billToer;
    private $billToCity;
    private $bizInvoiceContent;
    private $settlementNum;
    private $settlementNakedPrice;
    private $settlementTaxPrice;
    private $billToParty;
    private $totalBatchInvoiceNakedAmount;
    private $invoiceNakedPrice;
    private $invoiceTaxPrice;
    private $totalBatchInvoiceTaxAmount;
    private $enterpriseRegPhone;
    private $deliveryType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.invoice.submitInvoiceApply";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate             = $invoiceDate;
        $this->apiParas["invoiceDate"] = $invoiceDate;
    }

    public function getMarkId()
    {
        return $this->markId;
    }

    public function setMarkId($markId)
    {
        $this->markId             = $markId;
        $this->apiParas["markId"] = $markId;
    }

    public function getInvoiceOrg()
    {
        return $this->invoiceOrg;
    }

    public function setInvoiceOrg($invoiceOrg)
    {
        $this->invoiceOrg             = $invoiceOrg;
        $this->apiParas["invoiceOrg"] = $invoiceOrg;
    }

    public function getTotalBatchInvoiceAmount()
    {
        return $this->totalBatchInvoiceAmount;
    }

    public function setTotalBatchInvoiceAmount($totalBatchInvoiceAmount)
    {
        $this->totalBatchInvoiceAmount             = $totalBatchInvoiceAmount;
        $this->apiParas["totalBatchInvoiceAmount"] = $totalBatchInvoiceAmount;
    }

    public function getBillToContact()
    {
        return $this->billToContact;
    }

    public function setBillToContact($billToContact)
    {
        $this->billToContact             = $billToContact;
        $this->apiParas["billToContact"] = $billToContact;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title             = $title;
        $this->apiParas["title"] = $title;
    }

    public function getCurrentBatch()
    {
        return $this->currentBatch;
    }

    public function setCurrentBatch($currentBatch)
    {
        $this->currentBatch             = $currentBatch;
        $this->apiParas["currentBatch"] = $currentBatch;
    }

    public function getEnterpriseTaxpayer()
    {
        return $this->enterpriseTaxpayer;
    }

    public function setEnterpriseTaxpayer($enterpriseTaxpayer)
    {
        $this->enterpriseTaxpayer             = $enterpriseTaxpayer;
        $this->apiParas["enterpriseTaxpayer"] = $enterpriseTaxpayer;
    }

    public function getBillingType()
    {
        return $this->billingType;
    }

    public function setBillingType($billingType)
    {
        $this->billingType             = $billingType;
        $this->apiParas["billingType"] = $billingType;
    }

    public function getSettlementId()
    {
        return $this->settlementId;
    }

    public function setSettlementId($settlementId)
    {
        $this->settlementId             = $settlementId;
        $this->apiParas["settlementId"] = $settlementId;
    }

    public function getInvoiceNum()
    {
        return $this->invoiceNum;
    }

    public function setInvoiceNum($invoiceNum)
    {
        $this->invoiceNum             = $invoiceNum;
        $this->apiParas["invoiceNum"] = $invoiceNum;
    }

    public function getEnterpriseRegAddress()
    {
        return $this->enterpriseRegAddress;
    }

    public function setEnterpriseRegAddress($enterpriseRegAddress)
    {
        $this->enterpriseRegAddress             = $enterpriseRegAddress;
        $this->apiParas["enterpriseRegAddress"] = $enterpriseRegAddress;
    }

    public function getBillToProvince()
    {
        return $this->billToProvince;
    }

    public function setBillToProvince($billToProvince)
    {
        $this->billToProvince             = $billToProvince;
        $this->apiParas["billToProvince"] = $billToProvince;
    }

    public function getIsMerge()
    {
        return $this->isMerge;
    }

    public function setIsMerge($isMerge)
    {
        $this->isMerge             = $isMerge;
        $this->apiParas["isMerge"] = $isMerge;
    }

    public function getEnterpriseBankName()
    {
        return $this->enterpriseBankName;
    }

    public function setEnterpriseBankName($enterpriseBankName)
    {
        $this->enterpriseBankName             = $enterpriseBankName;
        $this->apiParas["enterpriseBankName"] = $enterpriseBankName;
    }

    public function getBillToCounty()
    {
        return $this->billToCounty;
    }

    public function setBillToCounty($billToCounty)
    {
        $this->billToCounty             = $billToCounty;
        $this->apiParas["billToCounty"] = $billToCounty;
    }

    public function getEnterpriseBankAccount()
    {
        return $this->enterpriseBankAccount;
    }

    public function setEnterpriseBankAccount($enterpriseBankAccount)
    {
        $this->enterpriseBankAccount             = $enterpriseBankAccount;
        $this->apiParas["enterpriseBankAccount"] = $enterpriseBankAccount;
    }

    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType             = $invoiceType;
        $this->apiParas["invoiceType"] = $invoiceType;
    }

    public function getBillToTown()
    {
        return $this->billToTown;
    }

    public function setBillToTown($billToTown)
    {
        $this->billToTown             = $billToTown;
        $this->apiParas["billToTown"] = $billToTown;
    }

    public function getBillToAddress()
    {
        return $this->billToAddress;
    }

    public function setBillToAddress($billToAddress)
    {
        $this->billToAddress             = $billToAddress;
        $this->apiParas["billToAddress"] = $billToAddress;
    }

    public function getPoNo()
    {
        return $this->poNo;
    }

    public function setPoNo($poNo)
    {
        $this->poNo             = $poNo;
        $this->apiParas["poNo"] = $poNo;
    }

    public function getInvoicePrice()
    {
        return $this->invoicePrice;
    }

    public function setInvoicePrice($invoicePrice)
    {
        $this->invoicePrice             = $invoicePrice;
        $this->apiParas["invoicePrice"] = $invoicePrice;
    }

    public function getSupplierOrder()
    {
        return $this->supplierOrder;
    }

    public function setSupplierOrder($supplierOrder)
    {
        $this->supplierOrder             = $supplierOrder;
        $this->apiParas["supplierOrder"] = $supplierOrder;
    }

    public function getRepaymentDate()
    {
        return $this->repaymentDate;
    }

    public function setRepaymentDate($repaymentDate)
    {
        $this->repaymentDate             = $repaymentDate;
        $this->apiParas["repaymentDate"] = $repaymentDate;
    }

    public function getInvoiceRemark()
    {
        return $this->invoiceRemark;
    }

    public function setInvoiceRemark($invoiceRemark)
    {
        $this->invoiceRemark             = $invoiceRemark;
        $this->apiParas["invoiceRemark"] = $invoiceRemark;
    }

    public function getTotalBatch()
    {
        return $this->totalBatch;
    }

    public function setTotalBatch($totalBatch)
    {
        $this->totalBatch             = $totalBatch;
        $this->apiParas["totalBatch"] = $totalBatch;
    }

    public function getBillToer()
    {
        return $this->billToer;
    }

    public function setBillToer($billToer)
    {
        $this->billToer             = $billToer;
        $this->apiParas["billToer"] = $billToer;
    }

    public function getBillToCity()
    {
        return $this->billToCity;
    }

    public function setBillToCity($billToCity)
    {
        $this->billToCity             = $billToCity;
        $this->apiParas["billToCity"] = $billToCity;
    }

    public function getBizInvoiceContent()
    {
        return $this->bizInvoiceContent;
    }

    public function setBizInvoiceContent($bizInvoiceContent)
    {
        $this->bizInvoiceContent             = $bizInvoiceContent;
        $this->apiParas["bizInvoiceContent"] = $bizInvoiceContent;
    }

    public function getSettlementNum()
    {
        return $this->settlementNum;
    }

    public function setSettlementNum($settlementNum)
    {
        $this->settlementNum             = $settlementNum;
        $this->apiParas["settlementNum"] = $settlementNum;
    }

    public function getSettlementNakedPrice()
    {
        return $this->settlementNakedPrice;
    }

    public function setSettlementNakedPrice($settlementNakedPrice)
    {
        $this->settlementNakedPrice             = $settlementNakedPrice;
        $this->apiParas["settlementNakedPrice"] = $settlementNakedPrice;
    }

    public function getSettlementTaxPrice()
    {
        return $this->settlementTaxPrice;
    }

    public function setSettlementTaxPrice($settlementTaxPrice)
    {
        $this->settlementTaxPrice             = $settlementTaxPrice;
        $this->apiParas["settlementTaxPrice"] = $settlementTaxPrice;
    }

    public function getBillToParty()
    {
        return $this->billToParty;
    }

    public function setBillToParty($billToParty)
    {
        $this->billToParty             = $billToParty;
        $this->apiParas["billToParty"] = $billToParty;
    }

    public function getTotalBatchInvoiceNakedAmount()
    {
        return $this->totalBatchInvoiceNakedAmount;
    }

    public function setTotalBatchInvoiceNakedAmount($totalBatchInvoiceNakedAmount)
    {
        $this->totalBatchInvoiceNakedAmount             = $totalBatchInvoiceNakedAmount;
        $this->apiParas["totalBatchInvoiceNakedAmount"] = $totalBatchInvoiceNakedAmount;
    }

    public function getInvoiceNakedPrice()
    {
        return $this->invoiceNakedPrice;
    }

    public function setInvoiceNakedPrice($invoiceNakedPrice)
    {
        $this->invoiceNakedPrice             = $invoiceNakedPrice;
        $this->apiParas["invoiceNakedPrice"] = $invoiceNakedPrice;
    }

    public function getInvoiceTaxPrice()
    {
        return $this->invoiceTaxPrice;
    }

    public function setInvoiceTaxPrice($invoiceTaxPrice)
    {
        $this->invoiceTaxPrice             = $invoiceTaxPrice;
        $this->apiParas["invoiceTaxPrice"] = $invoiceTaxPrice;
    }

    public function getTotalBatchInvoiceTaxAmount()
    {
        return $this->totalBatchInvoiceTaxAmount;
    }

    public function setTotalBatchInvoiceTaxAmount($totalBatchInvoiceTaxAmount)
    {
        $this->totalBatchInvoiceTaxAmount             = $totalBatchInvoiceTaxAmount;
        $this->apiParas["totalBatchInvoiceTaxAmount"] = $totalBatchInvoiceTaxAmount;
    }

    public function getEnterpriseRegPhone()
    {
        return $this->enterpriseRegPhone;
    }

    public function setEnterpriseRegPhone($enterpriseRegPhone)
    {
        $this->enterpriseRegPhone             = $enterpriseRegPhone;
        $this->apiParas["enterpriseRegPhone"] = $enterpriseRegPhone;
    }

    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType             = $deliveryType;
        $this->apiParas["deliveryType"] = $deliveryType;
    }
}
        
 
