<?php

namespace JdVopClient\request;

class VopGoodsQueryAreaStockStatesRequest
{
    private $apiParas = array();
    private $version;
    private $stockState;
    private $areaLevel;
    private $skuId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.queryAreaStockStates";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getStockState()
    {
        return $this->stockState;
    }

    public function setStockState($stockState)
    {
        $this->stockState             = $stockState;
        $this->apiParas["stockState"] = $stockState;
    }

    public function getAreaLevel()
    {
        return $this->areaLevel;
    }

    public function setAreaLevel($areaLevel)
    {
        $this->areaLevel             = $areaLevel;
        $this->apiParas["areaLevel"] = $areaLevel;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->skuId             = $skuId;
        $this->apiParas["skuId"] = $skuId;
    }
}
        
 
