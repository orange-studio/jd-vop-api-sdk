<?php

namespace JdVopClient\request;

class VopOrderApplyJdSettlementNumberRequest
{
    private $apiParas = array();
    private $version;
    private $outSettleNo;
    private $businessCode;
    private $payMoney;
    private $evidenceType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.applyJdSettlementNumber";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getOutSettleNo()
    {
        return $this->outSettleNo;
    }

    public function setOutSettleNo($outSettleNo)
    {
        $this->outSettleNo             = $outSettleNo;
        $this->apiParas["outSettleNo"] = $outSettleNo;
    }

    public function getBusinessCode()
    {
        return $this->businessCode;
    }

    public function setBusinessCode($businessCode)
    {
        $this->businessCode             = $businessCode;
        $this->apiParas["businessCode"] = $businessCode;
    }

    public function getPayMoney()
    {
        return $this->payMoney;
    }

    public function setPayMoney($payMoney)
    {
        $this->payMoney             = $payMoney;
        $this->apiParas["payMoney"] = $payMoney;
    }

    public function getEvidenceType()
    {
        return $this->evidenceType;
    }

    public function setEvidenceType($evidenceType)
    {
        $this->evidenceType             = $evidenceType;
        $this->apiParas["evidenceType"] = $evidenceType;
    }
}
        
 
