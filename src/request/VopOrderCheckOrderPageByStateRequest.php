<?php

namespace JdVopClient\request;

class VopOrderCheckOrderPageByStateRequest
{
    private $apiParas = array();
    private $version;
    private $startDate;
    private $pageSize;
    private $endDate;
    private $pageIndex;
    private $jdOrderIdIndex;
    private $checkOrderType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.checkOrderPageByState";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate             = $startDate;
        $this->apiParas["startDate"] = $startDate;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate             = $endDate;
        $this->apiParas["endDate"] = $endDate;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex             = $pageIndex;
        $this->apiParas["pageIndex"] = $pageIndex;
    }

    public function getJdOrderIdIndex()
    {
        return $this->jdOrderIdIndex;
    }

    public function setJdOrderIdIndex($jdOrderIdIndex)
    {
        $this->jdOrderIdIndex             = $jdOrderIdIndex;
        $this->apiParas["jdOrderIdIndex"] = $jdOrderIdIndex;
    }

    public function getCheckOrderType()
    {
        return $this->checkOrderType;
    }

    public function setCheckOrderType($checkOrderType)
    {
        $this->checkOrderType             = $checkOrderType;
        $this->apiParas["checkOrderType"] = $checkOrderType;
    }
}
        
 
