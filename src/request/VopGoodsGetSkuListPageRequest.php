<?php

namespace JdVopClient\request;

class VopGoodsGetSkuListPageRequest
{
    private $apiParas = array();
    private $version;
    private $bizPoolId;
    private $pageIndex;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.getSkuListPage";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getBizPoolId()
    {
        return $this->bizPoolId;
    }

    public function setBizPoolId($bizPoolId)
    {
        $this->bizPoolId             = $bizPoolId;
        $this->apiParas["bizPoolId"] = $bizPoolId;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex             = $pageIndex;
        $this->apiParas["pageIndex"] = $pageIndex;
    }
}

 
