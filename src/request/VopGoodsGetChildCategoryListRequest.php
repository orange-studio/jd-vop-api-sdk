<?php

namespace JdVopClient\request;

class VopGoodsGetChildCategoryListRequest
{
    private $apiParas = array();
    private $version;
    private $parentCategoryId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.getChildCategoryList";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getParentCategoryId()
    {
        return $this->parentCategoryId;
    }

    public function setParentCategoryId($parentCategoryId)
    {
        $this->parentCategoryId             = $parentCategoryId;
        $this->apiParas["parentCategoryId"] = $parentCategoryId;
    }
}
        
 
