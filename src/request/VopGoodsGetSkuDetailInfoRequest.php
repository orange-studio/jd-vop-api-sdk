<?php

namespace JdVopClient\request;

class VopGoodsGetSkuDetailInfoRequest
{
    private $apiParas = array();
    private $version;
    private $skuId;
    private $queryExtSet;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.getSkuDetailInfo";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getSkuId()
    {
        return $this->skuId;
    }

    public function setSkuId($skuId)
    {
        $this->skuId             = $skuId;
        $this->apiParas["skuId"] = $skuId;
    }

    public function getQueryExtSet()
    {
        return $this->queryExtSet;
    }

    public function setQueryExtSet($queryExtSet)
    {
        $this->queryExtSet             = $queryExtSet;
        $this->apiParas["queryExtSet"] = $queryExtSet;
    }
}
        
 
