<?php

namespace JdVopClient\request;

class VopAfsCancelAfsApplyRequest
{
    private $apiParas = array();
    private $version;
    private $thirdApplyId;
    private $remark;
    private $orderId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.afs.cancelAfsApply";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->thirdApplyId             = $thirdApplyId;
        $this->apiParas["thirdApplyId"] = $thirdApplyId;
    }

    public function getRemark()
    {
        return $this->remark;
    }

    public function setRemark($remark)
    {
        $this->remark             = $remark;
        $this->apiParas["remark"] = $remark;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId             = $orderId;
        $this->apiParas["orderId"] = $orderId;
    }
}
        
 
