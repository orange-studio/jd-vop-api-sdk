<?php

namespace JdVopClient\request;

class VopInvoiceGetInvoiceLogisticsInformationRequest
{
    private $apiParas = array();
    private $version;
    private $invoiceCode;
    private $invoiceId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.invoice.getInvoiceLogisticsInformation";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getInvoiceCode()
    {
        return $this->invoiceCode;
    }

    public function setInvoiceCode($invoiceCode)
    {
        $this->invoiceCode             = $invoiceCode;
        $this->apiParas["invoiceCode"] = $invoiceCode;
    }

    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId             = $invoiceId;
        $this->apiParas["invoiceId"] = $invoiceId;
    }
}
        
 
