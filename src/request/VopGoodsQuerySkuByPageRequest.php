<?php

namespace JdVopClient\request;

class VopGoodsQuerySkuByPageRequest
{
    private $apiParas = array();
    private $version;
    private $bizPoolId;
    private $offset;
    private $pageSize;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.querySkuByPage";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getBizPoolId()
    {
        return $this->bizPoolId;
    }

    public function setBizPoolId($bizPoolId)
    {
        $this->bizPoolId             = $bizPoolId;
        $this->apiParas["bizPoolId"] = $bizPoolId;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset             = $offset;
        $this->apiParas["offset"] = $offset;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }
}
        
 
