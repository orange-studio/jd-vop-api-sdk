<?php

namespace JdVopClient\request;

class VopAddressConvertFourAreaByDetailStrRequest
{
    private $apiParas = array();
    private $version;
    private $addressDetailStr;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.address.convertFourAreaByDetailStr";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getAddressDetailStr()
    {
        return $this->addressDetailStr;
    }

    public function setAddressDetailStr($addressDetailStr)
    {
        $this->addressDetailStr             = $addressDetailStr;
        $this->apiParas["addressDetailStr"] = $addressDetailStr;
    }
}
        
 
