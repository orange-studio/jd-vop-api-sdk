<?php

namespace JdVopClient\request;

class VopGoodsSearchSkuRequest
{
    private $apiParas = array();
    private $version;
    private $brandId;
    private $brandCollect;
    private $extAttrCollect;
    private $keyword;
    private $pageSize;
    private $priceCollect;
    private $useCacheStore;
    private $maxPrice;
    private $pageIndex;
    private $categoryId3;
    private $categoryId1;
    private $categoryId2;
    private $needMergeSku;
    private $sortType;
    private $minPrice;
    private $areaIds;
    private $sortExpandAttrs;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.goods.searchSku";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getBrandId()
    {
        return $this->brandId;
    }

    public function setBrandId($brandId)
    {
        $this->brandId             = $brandId;
        $this->apiParas["brandId"] = $brandId;
    }

    public function getBrandCollect()
    {
        return $this->brandCollect;
    }

    public function setBrandCollect($brandCollect)
    {
        $this->brandCollect             = $brandCollect;
        $this->apiParas["brandCollect"] = $brandCollect;
    }

    public function getExtAttrCollect()
    {
        return $this->extAttrCollect;
    }

    public function setExtAttrCollect($extAttrCollect)
    {
        $this->extAttrCollect             = $extAttrCollect;
        $this->apiParas["extAttrCollect"] = $extAttrCollect;
    }

    public function getKeyword()
    {
        return $this->keyword;
    }

    public function setKeyword($keyword)
    {
        $this->keyword             = $keyword;
        $this->apiParas["keyword"] = $keyword;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }

    public function getPriceCollect()
    {
        return $this->priceCollect;
    }

    public function setPriceCollect($priceCollect)
    {
        $this->priceCollect             = $priceCollect;
        $this->apiParas["priceCollect"] = $priceCollect;
    }

    public function getUseCacheStore()
    {
        return $this->useCacheStore;
    }

    public function setUseCacheStore($useCacheStore)
    {
        $this->useCacheStore             = $useCacheStore;
        $this->apiParas["useCacheStore"] = $useCacheStore;
    }

    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice             = $maxPrice;
        $this->apiParas["maxPrice"] = $maxPrice;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex             = $pageIndex;
        $this->apiParas["pageIndex"] = $pageIndex;
    }

    public function getCategoryId3()
    {
        return $this->categoryId3;
    }

    public function setCategoryId3($categoryId3)
    {
        $this->categoryId3             = $categoryId3;
        $this->apiParas["categoryId3"] = $categoryId3;
    }

    public function getCategoryId1()
    {
        return $this->categoryId1;
    }

    public function setCategoryId1($categoryId1)
    {
        $this->categoryId1             = $categoryId1;
        $this->apiParas["categoryId1"] = $categoryId1;
    }

    public function getCategoryId2()
    {
        return $this->categoryId2;
    }

    public function setCategoryId2($categoryId2)
    {
        $this->categoryId2             = $categoryId2;
        $this->apiParas["categoryId2"] = $categoryId2;
    }

    public function getNeedMergeSku()
    {
        return $this->needMergeSku;
    }

    public function setNeedMergeSku($needMergeSku)
    {
        $this->needMergeSku             = $needMergeSku;
        $this->apiParas["needMergeSku"] = $needMergeSku;
    }

    public function getSortType()
    {
        return $this->sortType;
    }

    public function setSortType($sortType)
    {
        $this->sortType             = $sortType;
        $this->apiParas["sortType"] = $sortType;
    }

    public function getMinPrice()
    {
        return $this->minPrice;
    }

    public function setMinPrice($minPrice)
    {
        $this->minPrice             = $minPrice;
        $this->apiParas["minPrice"] = $minPrice;
    }

    public function getAreaIds()
    {
        return $this->areaIds;
    }

    public function setAreaIds($areaIds)
    {
        $this->areaIds             = $areaIds;
        $this->apiParas["areaIds"] = $areaIds;
    }

    public function getSortExpandAttrs()
    {
        return $this->sortExpandAttrs;
    }

    public function setSortExpandAttrs($sortExpandAttrs)
    {
        $this->sortExpandAttrs             = $sortExpandAttrs;
        $this->apiParas["sortExpandAttrs"] = $sortExpandAttrs;
    }
}

 
