<?php

namespace JdVopClient\request;

class VopOrderQueryBillsOrderDetailsRequest
{
    private $apiParas = array();
    private $version;
    private $pageIndex;
    private $pageSize;
    private $billNo;
    private $multiPinSource;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.order.queryBillsOrderDetails";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex             = $pageIndex;
        $this->apiParas["pageIndex"] = $pageIndex;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }

    public function getBillNo()
    {
        return $this->billNo;
    }

    public function setBillNo($billNo)
    {
        $this->billNo             = $billNo;
        $this->apiParas["billNo"] = $billNo;
    }

    public function getMultiPinSource()
    {
        return $this->multiPinSource;
    }

    public function setMultiPinSource($multiPinSource)
    {
        $this->multiPinSource             = $multiPinSource;
        $this->apiParas["multiPinSource"] = $multiPinSource;
    }
}
        
 
