<?php

namespace JdVopClient\request;

class VopAfsQueryLogicticsInfoRequest
{
    private $apiParas = array();
    private $version;
    private $thirdApplyId;
    private $pageNo;
    private $originalOrderId;
    private $pageSize;
    private $orderId;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.afs.queryLogicticsInfo";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getThirdApplyId()
    {
        return $this->thirdApplyId;
    }

    public function setThirdApplyId($thirdApplyId)
    {
        $this->thirdApplyId             = $thirdApplyId;
        $this->apiParas["thirdApplyId"] = $thirdApplyId;
    }

    public function getPageNo()
    {
        return $this->pageNo;
    }

    public function setPageNo($pageNo)
    {
        $this->pageNo             = $pageNo;
        $this->apiParas["pageNo"] = $pageNo;
    }

    public function getOriginalOrderId()
    {
        return $this->originalOrderId;
    }

    public function setOriginalOrderId($originalOrderId)
    {
        $this->originalOrderId             = $originalOrderId;
        $this->apiParas["originalOrderId"] = $originalOrderId;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function setPageSize($pageSize)
    {
        $this->pageSize             = $pageSize;
        $this->apiParas["pageSize"] = $pageSize;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId             = $orderId;
        $this->apiParas["orderId"] = $orderId;
    }
}
        
 
