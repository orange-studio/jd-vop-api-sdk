<?php

namespace JdVopClient\request;

class VopAfsGetComponentUrlRequest
{
    private $apiParas = array();
    private $version;
    private $ext;
    private $componentType;

    public function getApiMethodName(): string
    {
        return "jingdong.vop.afs.getComponentUrl";
    }

    public function getApiParas()
    {
        if (empty($this->apiParas)) {
            return "{}";
        }
        return json_encode($this->apiParas);
    }

    public function check()
    {
    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key           = $value;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getExt()
    {
        return $this->ext;
    }

    public function setExt($ext)
    {
        $this->ext             = $ext;
        $this->apiParas["ext"] = $ext;
    }

    public function getComponentType()
    {
        return $this->componentType;
    }

    public function setComponentType($componentType)
    {
        $this->componentType             = $componentType;
        $this->apiParas["componentType"] = $componentType;
    }
}
        
 
