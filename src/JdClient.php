<?php

namespace JdVopClient;

use Exception;

class JdClient
{
    public  $serverUrl      = "https://api.jd.com/routerjson";
    public  $accessToken;
    public  $connectTimeout = 0;
    public  $readTimeout    = 0;
    public  $appKey;
    public  $appSecret;
    public  $version        = "2.0";
    public  $format         = "json";
    private $json_param_key = "360buy_param_json";

    /**
     * @throws Exception
     */
    public function curl($url, $postFields = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($this->readTimeout) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->readTimeout);
        }
        if ($this->connectTimeout) {
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        }
        if (strlen($url) > 5 && strtolower(substr($url, 0, 5)) == "https") {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        if (is_array($postFields) && 0 < count($postFields)) {
            $postBodyString = "";
            $postMultipart  = false;
            foreach ($postFields as $k => $v) {
                if ("@" != substr($v, 0, 1)) {
                    $postBodyString .= "$k=" . urlencode($v) . "&";
                } else {
                    $postMultipart = true;
                }
            }
            unset($k, $v);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($postMultipart) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString, 0, -1));
            }
        }
        $reponse = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch), 0);
        } else {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode) {
                throw new Exception($reponse, $httpStatusCode);
            }
        }
        curl_close($ch);
        return $reponse;
    }

    public function execute($request, $access_token = null)
    {
        $sysParams["app_key"]   = $this->appKey;
        $version                = $request->getVersion();
        $sysParams["v"]         = empty($version) ? $this->version : $version;
        $sysParams["method"]    = $request->getApiMethodName();
        $sysParams["timestamp"] = date("Y-m-d H:i:s", time());
        if (null != $access_token) {
            $sysParams["access_token"] = $access_token;
        }
        $apiParams                        = $request->getApiParas();
        $sysParams[$this->json_param_key] = $apiParams;
        $sysParams["sign"]                = $this->generateSign($sysParams);
        $requestUrl                       = $this->serverUrl . "?";
        foreach ($sysParams as $sysParamKey => $sysParamValue) {
            $requestUrl .= "$sysParamKey=" . urlencode($sysParamValue) . "&";
        }
        try {
            $resp = $this->curl($requestUrl, $apiParams);
        } catch (Exception $e) {
            $result->code = $e->getCode();
            $result->msg  = $e->getMessage();
            return $result;
        }
        $respWellFormed = false;
        if ("json" == $this->format) {
            $respObject = json_decode($resp);
            if (null !== $respObject) {
                $respWellFormed = true;
            }
        } else {
            if ("xml" == $this->format) {
                $respObject = @simplexml_load_string($resp);
                if (false !== $respObject) {
                    $respWellFormed = true;
                }
            }
        }
        if (false === $respWellFormed) {
            $result->code = 0;
            $result->msg  = "HTTP_RESPONSE_NOT_WELL_FORMED";
            return $result;
        }
        return $respObject;
    }

    protected function generateSign($params): string
    {
        ksort($params);
        $stringToBeSigned = $this->appSecret;
        foreach ($params as $k => $v) {
            if ("@" != substr($v, 0, 1)) {
                $stringToBeSigned .= "$k$v";
            }
        }
        unset($k, $v);
        $stringToBeSigned .= $this->appSecret;
        return strtoupper(md5($stringToBeSigned));
    }
}